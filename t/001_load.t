# -*- perl -*-

# t/001_load.t - check module loading and create testing directory

use Test::More tests => 2;

BEGIN { use_ok( 'Catalyst::Authentication::Credential::Google::OAuth2' ); }

my $object = Catalyst::Authentication::Credential::Google::OAuth2->new ();
isa_ok ($object, 'Catalyst::Authentication::Credential::Google::OAuth2');


